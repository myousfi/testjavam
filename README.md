<h1>Les instructions pour utiliser le projet:</h1>

<h3>1)Récupérer le projet depuis gitlab (Vous pouvez aussi modifier dans le projet): <br /></h3>

Command line instructions: <br />

Git global setup: <br />
git config --global user.name "nom"<br />
git config --global user.email "email@gmail.com"<br />

Create a new repository
git clone https://gitlab.com/myousfi/testjavam.git<br />
cd testjavam<br />
touch file.java<br />
git add file.java<br />
git commit -m "first commit"<br />
git push -u origin master<br />

<h3>2)Pour lancer le projet, vous devez  accéder au fichier App.java dans le  répertoire suivant: <br /></h3>
TestJavaM\src\main\java\com\adenom\TestJavaM \App.java, puis lancer la classe App.java <br />
pour voir l'affichage de l'exemple d'entrée suivante: partition([1,2,3,4,5], 2) <br /> 

<h3>3)Pour lancer les tests unitaires:</h3><br />
Lancer la classe AppTest.java dans le répertoire suivant:TestJavaM\src\main\java\com\adenom\TestJavaM\AppTesT.java.<br /> 

