package com.adenom.TestJavaM;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author myousfi
 *
 */
public class App 
{
	/**
	 * Lancer le main pour voir l'affichage de l'exemple d'entrée suivant:
	 * partition([1,2,3,4,5], 2) 
	 * @param args
	 */
    public static void main( String[] args )
    {   List<Object> liste= new ArrayList<Object>();
    	liste.add(1);
    	liste.add(2);
    	liste.add(3);
    	liste.add(4);
    	liste.add(5);
    	ArrayList<ArrayList<Object>> listeFinale= App.partition((ArrayList<Object>) liste, 2);
        System.out.println( "le resultat de la fonction partition([1,2,3,4,5], 2) est la suivant:\n"+listeFinale );
    }
    
    /**
     * fonction « partition » qui prend un paramètre « liste » et un paramètre « taille » 
     * et retourne une liste de sous liste, où chaque sous liste a au maximum « taille » éléments
     * @param liste
     * @param taille
     * @return une liste de sous liste, où chaque sous liste a au maximum « taille » éléments.
     */
    @SuppressWarnings("unchecked")
	public static ArrayList<ArrayList<Object>> partition (ArrayList<Object> liste, Integer taille){
    	 
		ArrayList<ArrayList<Object>> listeFinale = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> sousListe = new ArrayList<Object>();
      if ( liste!=null && ! liste.isEmpty() && taille !=null && taille >0) {
		int i=0; 
		while (i<liste.size())
		{
			sousListe.clear();
			for (int j = 0; j < taille; j++) 
			{
				if (i+j < liste.size())
				sousListe.add(liste.get(i+j));
			}

			listeFinale.add((ArrayList<Object>) sousListe.clone());
			i=i+taille;
			
		}
      }
		return listeFinale;
	}
}
