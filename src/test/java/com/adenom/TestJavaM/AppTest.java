package com.adenom.TestJavaM;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
    	super( testName );
    	
    	
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Test avec une liste d'entier 
     * partition([1,2,3,4,5], 2) retourne: [ [1,2], [3,4], [5] ]
     */
    public void testAppInt()
    {
    	List<Object> liste= new ArrayList<Object>();
    	liste.add(1);
    	liste.add(2);
    	liste.add(3);
    	liste.add(4);
    	liste.add(5);
    	ArrayList<ArrayList<Object>> listeFinale =App.partition((ArrayList<Object>) liste, 2);
    	ArrayList<ArrayList<Object>> listeAttendu = new ArrayList<ArrayList<Object>>();
    	ArrayList<Object> l1=  new ArrayList<Object>();
    	l1.add(1);
    	l1.add(2);
    	listeAttendu.add(l1);
    	ArrayList<Object> l2=  new ArrayList<Object>();
    	l2.add(3);
    	l2.add(4);
    	listeAttendu.add(l2);
     	ArrayList<Object> l3=  new ArrayList<Object>();
    	l3.add(5);
    	listeAttendu.add(l3);
    	assertEquals(listeFinale, listeAttendu);
    	assertEquals(listeFinale.size(), 3);
    }
    
    /**
     * Test avec une liste de chaine de caractere  
     * partition(["1","2","3","4","5"], 2) retourne: [ ["1","2"], ["3","4"], ["5"] ]
     */
    public void testAppString()
    {
    	List<Object> liste= new ArrayList<Object>();
    	liste.add("1");
    	liste.add("2");
    	liste.add("3");
    	liste.add("4");
    	liste.add("5");
    	ArrayList<ArrayList<Object>> listeFinale =App.partition((ArrayList<Object>) liste, 2);
    	ArrayList<ArrayList<Object>> listeAttendu = new ArrayList<ArrayList<Object>>();
    	ArrayList<Object> l1=  new ArrayList<Object>();
    	l1.add("1");
    	l1.add("2");
    	listeAttendu.add(l1);
    	ArrayList<Object> l2=  new ArrayList<Object>();
    	l2.add("3");
    	l2.add("4");
    	listeAttendu.add(l2);
     	ArrayList<Object> l3=  new ArrayList<Object>();
    	l3.add("5");
    	listeAttendu.add(l3);
    	assertEquals(listeFinale, listeAttendu);
    	assertEquals(listeFinale.size(), 3);
    }
    
    
    /**
     * Test avec une liste de chaine de float  
     * partition(["1","2","3","4","5"], 2) retourne: [ ["1","2"], ["3","4"], ["5"] ]
     */
    public void testAppFloat()
    {
    	List<Object> liste= new ArrayList<Object>();
    	liste.add( 1.0);
    	liste.add(2.0);
    	liste.add(3.0);
    	liste.add(4.0);
    	liste.add(5.0);
    	ArrayList<ArrayList<Object>> listeFinale =App.partition((ArrayList<Object>) liste, 1);
    	ArrayList<ArrayList<Object>> listeAttendu = new ArrayList<ArrayList<Object>>();
    	ArrayList<Object> l1=  new ArrayList<Object>();
    	l1.add(1.0);
    	listeAttendu.add(l1);
    	ArrayList<Object> l2=  new ArrayList<Object>();
    	l2.add(2.0);
    	listeAttendu.add(l2);
     	ArrayList<Object> l3=  new ArrayList<Object>();
    	l3.add(3.0);
    	listeAttendu.add(l3);
    	ArrayList<Object> l4=  new ArrayList<Object>();
    	l4.add(4.0);
    	listeAttendu.add(l4);
     	ArrayList<Object> l5=  new ArrayList<Object>();
    	l5.add(5.0);
    	listeAttendu.add(l5);
    	assertEquals(listeFinale, listeAttendu);
    	assertEquals(listeFinale.size(), 5);
    }
    
    
    /**
     * Test avec une liste vide puis liste null
     */
    public void testListe()
    {
    	List<Object> liste= new ArrayList<Object>();
    	ArrayList<ArrayList<Object>> listeFinale =App.partition((ArrayList<Object>) liste, 2);
    	assertEquals(listeFinale.size(), 0);
    	assertNotNull(listeFinale);
    	List<Object> listeNull= null;
    	ArrayList<ArrayList<Object>> listeFinaleForNull =App.partition((ArrayList<Object>) listeNull, 2);
    	assertEquals(listeFinaleForNull.size(), 0);
    	assertNotNull(listeFinaleForNull);
  
    }
    
    /**
     * Test avec une taille à 0 puis taille null
     */
    public void testTaille()
    {
    	List<Object> liste= new ArrayList<Object>();
    	liste.add( 1.0);
    	liste.add(2.0);
    	liste.add(3.0);
    	liste.add(4.0);
    	liste.add(5.0);
    	ArrayList<ArrayList<Object>> listeFinale =App.partition((ArrayList<Object>) liste, 0);
    	assertEquals(listeFinale.size(), 0);
    	assertNotNull(listeFinale);
    
    	ArrayList<ArrayList<Object>> listeFinaleForNull =App.partition((ArrayList<Object>) liste, null);
    	assertEquals(listeFinaleForNull.size(), 0);
    	assertNotNull(listeFinaleForNull);
  
    }
}
